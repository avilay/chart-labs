Feature: Account Settings
	
Scenario: Register for the service
	Given I am a new visitor
	When I visit the dashboard website
	Then I'll see a landing page with a register button there.
	When I click on the register button
	Then I'll see a register form.
	When I fill out my name, email, and password on the register form
	And click on the register button
	Then I should be taken to the Accounts page with a how-this-works blurb

Scenario: First time logging in
	Given I am a new user on the home page
	And am using a new browser,
	When I click on the login button
	And fill out my email and password on the login form page
	And click on the login button
	Then I should be taken to the Accounts page with a how-this-works blurb
	And should see a flash message stating that I do not have any dashboards set up

Scenario: Logging into the dashboard by an old time user from a new browser
	Given I am an existing user on the home page
	And am using a new browser,
	When I click on the login button
	And fill out my email and password on the login form page
	And click on the login button
	Then I should be taken to my existing dashboard

Scenario: Logging into the dashboard by an old time user from their regular browser
	Given I am an existing user
	And am using my regular browser,
	When I visit the dashboard home page
	Then I am redirected to my existing dashboard








