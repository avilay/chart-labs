import shutil
from datetime import datetime
import os.path as path
import glob
import os


prod_root = '/home/avilay/Dropbox/Apps/Heroku/web-chartlabs/'
src_root = '/home/avilay/projects/chart-labs/web-chart-labs'

basename = 'web-chartlabs_{}'.format(datetime.now().strftime('%Y-%m-%d'))
if path.exists(prod_root):
    shutil.make_archive(basename, 'zip', prod_root)
    shutil.rmtree(prod_root)

css_src = path.join(src_root, 'public', 'css')
css_dst = path.join(prod_root, 'public', 'css')
shutil.copytree(css_src, css_dst)


img_src = path.join(src_root, 'public', 'img')
img_dst = path.join(prod_root, 'public', 'img')
shutil.copytree(img_src, img_dst)

js_dst = path.join(prod_root, 'public', 'js')
if not path.exists(js_dst):
    os.mkdir(js_dst)
bundle_src = path.join(src_root, 'public', 'js', 'bundle.js')
bundle_dst = path.join(js_dst, 'bundle.js')
shutil.copyfile(bundle_src, bundle_dst)

html_files = glob.glob(path.join(src_root, 'public', '*.html'))
for html_file in html_files:
    dst = path.join(prod_root, 'public', path.basename(html_file))
    shutil.copy(html_file, dst)

ico_src = path.join(src_root, 'public', 'favicon.ico')
ico_dst = path.join(prod_root, 'public', 'favicon.ico')
shutil.copy(ico_src, ico_dst)

server_src = path.join(src_root, 'server.py')
server_dst = path.join(prod_root, 'server.py')
shutil.copy(server_src, server_dst)

procfile_src = path.join(src_root, 'Procfile')
procfile_dst = path.join(prod_root, 'Procfile')
shutil.copy(procfile_src, procfile_dst)

runtime_src = path.join(src_root, 'runtime.txt')
runtime_dst = path.join(prod_root, 'runtime.txt')
shutil.copy(runtime_src, runtime_dst)

reqms_src = path.join(src_root, 'requirements.txt')
reqms_dst = path.join(prod_root, 'requirements.txt')
shutil.copy(reqms_src, reqms_dst)
