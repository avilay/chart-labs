var Navbar = require('./Navbar.react');
var Login = require('./Login.react');
var Register = require('./Register.react');
var Dashboard = require('./Dashboard.react');
var DashboardList = require('./DashboardList.react');
var Report = require('./Report.react');
var sjcl = require('sjcl');
var React = require('react');
var RouterMixin  = require('react-mini-router').RouterMixin;
var navigate = require('react-mini-router').navigate;
var config = require('../config');
var jsapi = require('../jsapi');

google.load('visualization', '1.1', {'packages':['corechart']});

var ChartlabsApp = React.createClass({
	mixins: [RouterMixin],

	routes: {
		'/': 'default',
		'/login': 'login',
		'/logout': 'logout',
		'/register': 'register',
		'/dashboards': 'dashboards',
		'/dashboards/:id': 'dashboard',
		'/reports/:id': 'report',
		'/debug': 'debug',
		'/profile': 'underConstruction',
		// '/docs': 'underConstruction',
		// '/help': 'underConstruction',
		'/ise': 'serverError'
	},

	saveUserToLocalStorage: function (user) {
		window.localStorage.setItem('user', sjcl.encrypt(config.encKey, JSON.stringify(user)));
	},

	getUserFromLocalStorage: function () {
		var userCookie = window.localStorage.getItem('user');
		var user = {
			isLoggedIn: false,
			displayName: '',
			email: '',
			token: ''
		};
		if (userCookie) {
			user = JSON.parse(sjcl.decrypt(config.encKey, userCookie));			
		}
		return user;
	},

	removeUserFromLocalStorage: function () {
		window.localStorage.removeItem('user');
	},

	getInitialState: function () {
		return {
			user: this.getUserFromLocalStorage(),
			report: null
		};
	},

	handleLogin: function (err, user, remember) {	
		if (err) {
			if (err.status >= 500) {
				this.setState({path: '/ise'});
			} else {
				this.setState({
					user: this.getUserFromLocalStorage(),
					path: '/login?error=' + err.responseJSON._error.message				
				});
			}
		} else {
			if (remember) {
				this.saveUserToLocalStorage(user);
			}
			this.setState({
				path: '/',
				user: user
			});
		}
	},

	handleNewUser: function (err, user) {
		console.log('Inside ChartlabsApp.handleNewUser');		
		if (err) {
			if (err.status >= 500) {
				this.setState({path: '/ise'});
			} else {
				var error = err.responseJSON;
				var errMsg = '';
				if (error._issues) {
					flds = Object.keys(error._issues);
					for (var i = 0; i < flds.length; i++) {
						errMsg += flds[i] + ': ' + error._issues[flds[i]] + ' ';
					}
				} else {
					errMsg = error._error.message;
				}
				this.setState({
					user: this.getUserFromLocalStorage(),
					path: '/register?error=' + errMsg
				});
			}
		} else {
			this.setState({
				path: '/',
				user: user
			});
		}
	},

	componentDidUpdate: function (prevProps, prevState) {
		if (prevState.path !== '/logout' && this.state.path === '/logout') {
			this.removeUserFromLocalStorage();
			this.setState({user: this.getUserFromLocalStorage()});
		}
	},

	render: function () {
		console.debug('Lifecycle: ****** ChartlabsApp.render');
		return this.renderCurrentRoute();
	},

	debug: function () {
		console.debug('Route: debug');
		return (
			<div>
				<h1>Debug</h1>
				<button className="btn btn-default" onClick={this.handleDebug}>Log out handler</button> <br />
				<a href="/logout">Log out link</a>
			</div>
		);
	},

	default: function () {
		console.debug('Route: default');
		if (this.state.user.isLoggedIn) {
			navigate('/dashboards', true);
			return this.dashboards();
		} else {
			window.location.replace('/welcome.html');
		}
	},

	login: function (q) {
		console.debug('Route: login', q);
		return (
			<div>
				<Navbar userDisplayName={this.state.user.displayName} currentView='login' />
				<Login flash={q} onLogin={this.handleLogin} />
			</div>
		);
	},

	logout: function () {
		console.debug('Route: logout');
		return (
			<div>
				<Navbar userDisplayName='' currentView='login' />
				<div className="container">
					<h1>Thank You</h1>
					<div className="row">
						<div className="col-md-6">
							<p className="lead">
								You have been logged out of Chart Labs.
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	},

	register: function (q) {
		console.debug('Route: register');
		return (
			<div>
				<Navbar userDisplayName={this.state.user.displayName} currentView='register' />
				<Register flash={q} onNewUser={this.handleNewUser} />	
			</div>
		);
	},

	dashboards: function () {
		console.debug('Route: dashboards');
		if (this.state.user.isLoggedIn) {
			return (
				<div>
					<Navbar userDisplayName={this.state.user.displayName} currentView='dashboards' />
					<DashboardList basicAuthToken={this.state.user.token} />
				</div>
			);
		} else {
			navigate('/login', true);
			return this.login();
		}
	},

	dashboard: function (id) {
		console.debug('Route: dashboard');
		if (this.state.user.isLoggedIn) {
			return (
				<div>
					<Navbar userDisplayName={this.state.user.displayName} currentView='dashboard' onLogout={this.handleLogout} />
					<Dashboard dashboardId={id} basicAuthToken={this.state.user.token} />
				</div>
			);
		} else {
			navigate('/login', true);
			return this.login();
		}
	},

	report: function (id) {
		console.debug('Route: report');
		if (this.state.user.isLoggedIn) {
			return (
				<div>
					<Navbar userDisplayName={this.state.user.displayName} currentView='dashboard' onLogout={this.handleLogout} />
					<Report reportId={id} basicAuthToken={this.state.user.token} />
				</div>
			);
		} else {
			navigate('/login', true);
			return this.login();	
		}
	},

	underConstruction: function () {
		console.debug('Route: underConstruction');
		return (
			<div>
				<Navbar userDisplayName={this.state.user.displayName} currentView={this.state.path.substring(1)} onLogout={this.handleLogout} />
				<div className="container">
					<h1>Under Construction</h1>
					<div className="row">
						<div className="col-md-6">
							<p className="lead">
								This page is under construction.
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	},

	serverError: function () {
		console.debug('Route: Server error');
		return (
			<div>
				<Navbar userDisplayName={this.state.user.displayName} currentView={this.state.path.substring(1)} onLogout={this.handleLogout} />
				<div className="container">
					<h1>Oops!</h1>
					<div className="row">
						<div className="col-md-12">
							<p className="lead">
								Something went wrong on our servers! Our engineers are working tirelessly to fix the problem.
							</p>
							<p>
								To report this problem email report@avilaylabs.net.
							</p>
						</div>
					</div>
				</div>
			</div>	
		);
	}, 

	notFound: function (path) {
		console.debug('Route: Not found');
		return (
			<div>
				<Navbar userDisplayName={this.state.user.displayName} currentView={this.state.path.substring(1)} onLogout={this.handleLogout} />
				<div className="container">
					<h1>Not Found</h1>
					<div className="row">
						<div className="col-md-12">
							<p>
								{path} <br></br>
								This page does not exist!
							</p>
						</div>
					</div>
				</div>
			</div>	
		);
	}
});

module.exports = ChartlabsApp;