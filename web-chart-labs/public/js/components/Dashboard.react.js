var jsapi = require('../jsapi')
var React = require('react');
var moment = require('moment');
var ReportTile = require('./ReportTile.react');
var config = require('../config');

window._dashboard = null;
window._reports = null;

var Dashboard = React.createClass({
	getInitialState: function () {
		return {
			dashboard: null,
			reports: null
		};
	},

	componentDidMount: function () {
		console.debug('Lifecycle: Dashboard.componentDidMount');
		if (window._dashboard && window._dashboard._id === this.props.dashboardId) {
			console.log('Existing globals dashboard and reports. Not feteching.');
			this.setState({dashboard: window._dashboard, reports: window._reports});
		} else {
			var self = this;
			var url = config['apiEndpoint'] + '/dashboards/' + this.props.dashboardId;
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				cache: false,
				headers: {'Authorization': this.props.basicAuthToken}			
			})
			.then(function (res) {
				console.log('Got dashboards');
				var dashboard = {_id: '', name: ''};
				if (res._items && res._items.length > 0) {
					dashboard = res._items[res._items.length - 1]; 
				} else if (res) {
					dashboard = res;
				}
				window._dashboard = dashboard;
				var url = config['apiEndpoint'] + '/reports?where={"dashboard_id": "' + self.props.dashboardId + '"}';
				console.log('Fetching reports from url ', url);
				return $.ajax({
					url: url,
					type: 'GET',
					dataType: 'json',
					cache: false,
					headers: {'Authorization': self.props.basicAuthToken}
				});	
			})
			.then(function (res) {
				console.log('Setting state with dashboard and reports');
				window._reports = res._items;
				self.setState({
					dashboard: window._dashboard,
					reports: res._items
				});
			})
			.fail(function (xhr, status, err) {
				console.log('Error: ', err);
				console.log('Status: ', status);
				console.dir(xhr);
			});
		}
	},

	render: function () {
		console.debug('Lifecycle: Dashboard.render');
		var tilesPerRow = 3;
		var reportBands = [];

		if (! this.state.dashboard ) {
			return (<div></div>);
		}
		console.log('State was loaded. Proceeding with rendering');

		// Reshape the reports array into (n, 3) dims
		var numRows = Math.floor(this.state.reports.length / tilesPerRow);
		for (var row = 0; row < numRows; row++) {
			var ndx = row * tilesPerRow;
			var reportBand = 
				<div className="row" key={ndx}>
					<div className="col-md-4">
						<ReportTile report={this.state.reports[ndx]} />
					</div>
					<div className="col-md-4">
						<ReportTile report={this.state.reports[ndx + 1]} />
					</div>
					<div className="col-md-4">
						<ReportTile report={this.state.reports[ndx + 2]} />
					</div>					
				</div>
			reportBands.push(reportBand);
		}
		// When number of reports is not a multiple of 3, then the last row contains
		// the overflow
		var overflow = this.state.reports.length % tilesPerRow;
		var ndx = numRows * tilesPerRow;
		if (overflow === 1) {	
			var reportBand =
				<div className="row" key={ndx}>
					<div className="col-md-4">
						<ReportTile onClickReport={this.handleClickReport} report={this.state.reports[ndx]} key={ndx} />
					</div>
				</div>
			reportBands.push(reportBand);
		} else if (overflow === 2) {
			var reportBand = 
				<div className="row" key={ndx}>
					<div className="col-md-4">
						<ReportTile onClickReport={this.handleClickReport} report={this.state.reports[ndx]} key={ndx} />
					</div>
					<div className="col-md-4">
						<ReportTile onClickReport={this.handleClickReport} report={this.state.reports[ndx + 1]} key={ndx + 1} />
					</div>
				</div>
			reportBands.push(reportBand);
		}
		var uo = (new Date(this.state.dashboard._updated)).toLocaleString();
		return (
			<div className="container">
				<h1>{this.state.dashboard.name}</h1>
				<span className="text-small">Last updated on {uo}</span>
				{reportBands}
			</div>
		);
	}
});

module.exports = Dashboard;