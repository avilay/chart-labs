var React = require('react');
var chartHelper = require('../charts.js');


var ReportTile = React.createClass({
    drawChart: function () {
    	var report = this.props.report;
    	var elem = document.getElementById(report._id);
    	var data = chartHelper.getDataTable(report);
    	var options = chartHelper.getChartOptions(report._chart_type, report.header);
    	var chart = chartHelper.createChartObj(report._chart_type, elem);	
    	chart.draw(data, options);
    },

	componentDidMount: function () {
		if (this.props.report._is_chartable && this.props.report._chart_type !== 'label') {
			google.load('visualization', '1.1', {
				'packages':['corechart'], 
				'callback': this.drawChart
			});
		}
	},

	render: function () {
		var report = this.props.report;
		var link = '/reports/' + report._id;
		if (report._is_chartable) {
			if (report._chart_type === 'label') {
				return (
					<div className="lined">
                    	<h3 className="widget-title"><a href={link}>{report.title}</a></h3>
                    	<div id={report._id} className="text-metric">
                        	<div className="value">{report.data[0][0]}</div>
                        	<br></br>                       
                        	<div className="units">{report.header[0]}</div>
                    	</div>
                	</div>
				);
			} else {
				return (
					<div className="lined">
	                	<h3 className="widget-title"><a href={link}>{report.title}</a></h3>
	                	<div id={report._id}></div>
	            	</div>
				);
			}
		} else {
			return (
				<div className="lined">
	                <h3 className="widget-title"><a href={link}>{report.title}</a></h3>
                    <div className="text-metric">                        
                        <div><img src="/img/table.png"/></div>
                        <span>Click for details</span>
                    </div>
                </div>
            );
		}
	}
});

module.exports = ReportTile;