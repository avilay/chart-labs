var React = require('react');
var navigate = require('react-mini-router').navigate;
var chartHelper = require('../charts.js');
var config = require('../config');

var Chart = React.createClass({
	drawChart: function () {
    	var report = this.props.report;
    	var elem = document.getElementById("chart");
    	var data = chartHelper.getDataTable(report);
    	var options = chartHelper.getChartOptions(report._chart_type, report.header);
    	var chart = chartHelper.createChartObj(report._chart_type, elem);	
    	chart.draw(data, options);
    },

	componentDidMount: function () {
		var report = this.props.report;
		if (report && report._is_chartable && report._chart_type !== 'label') {
			google.load('visualization', '1.1', {
				'packages':['corechart'], 
				'callback': this.drawChart
			});
		}	
	},

	render: function () {
		return (
			<div className="row">            
            	<div className="col-md-12">
                	<div id="chart"></div>
            	</div>                        
        	</div>
		);
	}
});

var Row = React.createClass({
	render: function () {
		var row = this.props.row;
		var rownum = this.props.rownum;
		var cells = [];
		for (var i = 0; i < row.length; i++) {
			var cell = row[i];
			var key = (rownum + 1) * (i + 1);
			cells.push(<td key={key}>{cell}</td>);
		}
		return (<tr>{cells}</tr>);
	}
});

var Table = React.createClass({
	render: function () {
		var data = this.props.data;
		var header = this.props.header;
		var rows = [];
		for (var i = 0; i < data.length; i++) {
			var row = data[i];
			rows.push(<Row row={row} rownum={i} key={i} />);
		}
		return (
			<table className="table table-striped table-bordered table-condensed">
				<thead>
            		<tr>
                		{header.map(function (colName, idx) {
                			return <th key={idx}>{colName}</th>;
                		})}
            		</tr>
        		</thead>
				{rows}
			</table>
		);
	}

});


var Report = React.createClass({
	getInitialState: function () {
		return { report: null };
	},

	componentDidMount: function () {
		var report = null;
		if (window._reports) {
			for (var i = 0; i < window._reports.length; i++) {
				if (window._reports[i]._id === this.props.reportId) {
					report = window._reports[i];
					break;
				}
			}
		}
		if (report) {
			console.log('Found existing report. Not fetching');
			this.setState({report: report});
		} else {
			console.log('Report not found in cache. Fetching');
			var url = config['apiEndpoint'] + '/reports/' + this.props.reportId;
			var self = this;
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				cache: true,
				headers: {'Authorization': this.props.basicAuthToken}	
			})
			.done(function (res) {
				self.setState({report: res});
			})
			.fail(function (xhr, status, err) {
				if (err.status >= 500) {
					navigate('/ise');
				} else {
					navigate('/notfound');
				}
			});
		}
	},

	render: function () {
		console.debug('Lifecycle: Report.render');
		if (! this.state.report) {
			return (<div></div>);
		}
		var report = this.state.report;
		var data = report.data;
		var header = report.header;
		return (
			<div className="container">
				<h1>{report.title}</h1>
	    		<Chart report={report} />
	    		<div className="row new-row">
            		<div className="col-md-12">
                		<Table data={data} header={header} />
                    </div>
                </div>
	    	</div>
		);
	}
});

module.exports = Report;

