var React = require('react');
var moment = require('moment');
var config = require('../config');

var DashboardList = React.createClass({
	getInitialState: function () {
		return {
			dashboards: []
		};
	},

	handleDashboardDetail: function (e) {
		e.preventDefault();
		var dashboardId = e.target.dataset.dashboardid;
		this.props.onDashboardDetail(dashboardId);
	},

	componentDidMount: function () {
		var url = config['apiEndpoint'] + '/dashboards';
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			cache: false,
			headers: {'Authorization': this.props.basicAuthToken},
			success: function (res) {
				var dashboards = res._items;
				for (var i = 0; i < dashboards.length; i++) {
					var dashboard = dashboards[i];
					dashboard._created = new Date(dashboard._created);
					dashboard._updated = new Date(dashboard._updated);
				}
				this.setState({dashboards: dashboards});
			}.bind(this),
			error: function (xhr, status, errorThrown) {
				console.log('Error: ', errorThrown);
				console.log('Status: ', status);
				console.dir(xhr);
			}.bind(this)
		});
	},

	render: function () {
		console.debug('Lifecycle: DashboardList.render');
		var dashboardRows =  [];
		for (var i = 0; i < this.state.dashboards.length; i++) {
			var dashboard = this.state.dashboards[i];
			var uo = (new Date(dashboard._updated)).toLocaleString();
			var co = (new Date(dashboard._created)).toLocaleString();
			var link = '/dashboards/' + dashboard._id;
			var dashboardRow =
				<tr key={i}>
					<td><a href={link}>{dashboard.name}</a></td>
					<td>{uo}</td>
					<td>{co}</td>
				</tr>
			dashboardRows.push(dashboardRow);
		}

		return (
			<div className="container">
				<h1>My Dashboards</h1>
				<div className="row">
					<div className="col-md-6">
						<table className="table table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Updated On</th>
									<th>Created On&nbsp;&nbsp;<i className="fa fa-sort-asc"></i></th>
								</tr>
							</thead>
							<tbody>
								{dashboardRows}
							</tbody>
						</table>
					</div>
				</div>	
			</div>
		);
	}
});

module.exports = DashboardList;