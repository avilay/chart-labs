var React = require('react');
var config = require('../config');

var Register = React.createClass({
	handleNewUser: function (e) {
		e.preventDefault();
		var newUser = {
			display_name: React.findDOMNode(this.refs.displayName).value.trim(),
			email: React.findDOMNode(this.refs.email).value.trim(),
			password: React.findDOMNode(this.refs.password).value.trim()
		};
		var url = config.apiEndpoint + '/users/';
		var self = this;
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json; charset=UTF-8',
			processData: false,
			data: JSON.stringify(newUser)
		})
		.done(function (res) {
			var user = {
				isLoggedIn: true,
				displayName: newUser.display_name,
				email: newUser.email,
				token: 'Basic ' + btoa(newUser.email + ':' + newUser.password)
			};
			self.props.onNewUser(null, user);
		})
		.fail(function (xhr, status, err) {
			self.props.onNewUser(xhr, null);
		});
	},

	render: function () {
		console.debug('Lifecycle: Register.render');
		var flashMsg = '';
		if (this.props.flash && this.props.flash.error) {
			flashMsg =
				<div className="flash bg-danger"> 
					{this.props.flash.error}
				</div>
		}
		return (
			<div className="container">				
				<h1>Register</h1>				
				<div className="row">					
					<div className="col-md-4">
						{flashMsg}
						<form onSubmit={this.handleNewUser}>
							<div className="form-group">
								<label>Name</label>
								<input type="text" className="form-control" ref="displayName" />
							</div>
							<div className="form-group">
								<label>Email</label>
								<input type="email" className="form-control" ref="email" />
							</div>
							<div className="form-group">
								<label>Password</label>
								<input type="password" className="form-control" ref="password" />
							</div>
							<button type="submit" className="btn btn-success">
								Register
							</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = Register;
