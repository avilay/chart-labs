var React = require('react');
var config = require('../config');

var Navbar = React.createClass({
    render: function () {
        console.debug('Lifecycle: Navbar.render');
        var userDisplayName = this.props.userDisplayName;
        var currentView = this.props.currentView;
        var acctBtn = null;
        if (userDisplayName) {
            acctBtn = 
                <li>
                    <div className="btn-group">
                        <button type="button" className="btn btn-nav dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {userDisplayName} <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu">
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="/logout">Log Out</a></li>
                        </ul>
                    </div>
                </li>
        } else {
            acctBtn =
                <li> 
                    <div>
                        <a href="/login" className="btn btn-nav">Login</a>
                    </div>
                </li>
        }
        
        var dashboardsTab = null;
        if (currentView === 'dashboards') {
            dashboardsTab = <li className="active"><a href="/dashboards">My Dashboards</a></li>
        } else {
            dashboardsTab = <li><a href="/dashboards">My Dashboards</a></li>
        }
        
        return (
			<div>
				<nav className="navbar navbar-inverse navbar-fixed-top">
        			<div className="container">
            			<div className="navbar-header">
                			<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    			<span className="sr-only">Toggle navigation</span>
                    			<span className="icon-bar"></span>
                    			<span className="icon-bar"></span>
                    			<span className="icon-bar"></span>
                			</button>
                			<a className="navbar-brand" href="#">Chart Labs</a>
            			</div>
            			<div id="navbar" className="navbar-collapse collapse">
                			<ul className="nav navbar-nav navbar-right">
                    			{dashboardsTab}
                    			<li><a href={config['apiEndpoint'] + '/docs'}>Docs</a></li>
                                {acctBtn}
                			</ul>
            			</div>
        			</div>
    			</nav>
			</div>
		);
	}
});

module.exports = Navbar;