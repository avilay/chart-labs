var React = require('react');
var config = require('../config');

var Login = React.createClass({
	handleLogin: function (e) {
		e.preventDefault();
		var email = React.findDOMNode(this.refs.email).value.trim();
		var password = React.findDOMNode(this.refs.password).value.trim();
		var remember = React.findDOMNode(this.refs.remember).checked;
		var basicAuthToken = 'Basic ' + btoa(email + ':' + password);
		var self = this;
		var url = config.apiEndpoint + '/users/' + email;		
		$.ajax({
			url: url,
			type: 'GET',
			cache: false,
			dataType: 'json',
			headers: {'Authorization': basicAuthToken}
		})
		.done(function (loggedInUser) {
			var user = {
				isLoggedIn: true,
				displayName: loggedInUser.display_name,
				email: loggedInUser.email,
				token: basicAuthToken
			}; 
			self.props.onLogin(null, user, remember);
		})
		.fail(function (xhr, status, err) {
			console.dir(xhr);
			self.props.onLogin(xhr, null, null);
		});
	},

	render: function () {	
		console.debug('Lifecycle: Login.render', this.props.flash);
		var flashMsg = '';
		if (this.props.flash && this.props.flash.error) {
			flashMsg =
				<div className="flash bg-danger"> 
					{this.props.flash.error}
				</div>
		}
		return (
			<div className="container">
				<h1>Login</h1>				
				<div className="row">
					<div className="col-md-4">
						{flashMsg}
						<form onSubmit={this.handleLogin}>
							<div className="form-group">
								<label>Email</label>
								<input type="email" className="form-control" ref="email" />
							</div>
							<div className="form-group">
								<label>Password</label>
								<input type="password" className="form-control" ref="password" />
							</div>
							<div className="checkbox">
    							<label>
      								<input type="checkbox" ref="remember"/> Remember Me
    							</label>
  							</div>
							<button type="submit" className="btn btn-primary">
								Login
							</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = Login;
