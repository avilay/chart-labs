window.$ = window.jQuery = require('jquery');
require('bootstrap');
var url = require('url');
var React = require('react');
var ChartlabsApp = React.createFactory(require('./components/ChartlabsApp.react'));

React.render(
	ChartlabsApp({history: true}),
	document.getElementById('chartlabsapp')
);
