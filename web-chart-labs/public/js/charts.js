// ['label', 'timeseries_col', 'timeseries_line', 'scatter', 'pie', 'bar', 'stacked', 'line']

var chartHelper = {
	colors: ['#2980b9', '#e74c3c', '#8e44ad', '#27ae60', '#d35400', '#2c3e50', '#7f8c8d', '#f1c40f'],
	
	getChartOptions: function (chartType, header) {
		this.colors.push(this.colors.shift());	
		var options = {
			colors: this.colors,
			fontSize: 12,
			height: 300
		};
		if (chartType === 'timeseries_col') {
			options.chartArea = {width: '80%', height: '80%'};
			options.legend = {position: 'none'};
			options.hAxis = {textPosition: 'out', format: 'MM/dd'};
			options.vAxis = {textPosition: 'out'};
		} else if (chartType === 'timeseries_line') {
			options.chartArea = {width: '80%', height: '80%'};
			options.legend = { position: 'top', maxLines: 2 };
			options.hAxis = {textPosition: 'out', format: 'MM/dd'};
		} else if (chartType === 'scatter') {
			options.chartArea = {width: '80%', height: '80%'};
			options.legend = {position: 'none'};
			options.hAxis = { title: header[0] };
            options.vAxis = { title: header[1] };
		} else if (chartType === 'pie') {
			options.chartArea = {width: '90%', height: '90%'};
			options.pieSliceText = 'label';
			options.legend = 'none';
		} else if (chartType === 'bar') {
			options.chartArea = {width: '90%', height: '90%'};
			options.legend = {position: 'none'};
			options.hAxis = {textPosition: 'none'};
			options.vAxis = {textPosition: 'none'};
		} else if (chartType === 'stacked') {
			options.chartArea = {width: '80%', height: '80%'};
			options.legend = { position: 'top', maxLines: 2 };
            options.hAxis = {textPosition: 'out', format: 'MM/dd'};
            options.isStacked = 'percent';
		} else if (chartType === 'line') {
			options.chartArea = {width: '80%', height: '80%'};
			options.legend = { position: 'top', maxLines: 2 };
			options.hAxis = {textPosition: 'out'};
		} else {
			return null;
		}
		return options;
	},

	createChartObj: function (chartType, elem) {
		if (chartType === 'timeseries_col' || chartType === 'stacked') {
			return new google.visualization.ColumnChart(elem);	
		} else if (chartType === 'timeseries_line') {
			return new google.visualization.LineChart(elem);
		} else if (chartType === 'scatter') {
			return new google.visualization.ScatterChart(elem);
		} else if (chartType === 'pie') {
			return new google.visualization.PieChart(elem);
		} else if (chartType === 'bar') {
			return new google.visualization.BarChart(elem);
		} else if (chartType === 'line') {
			return new google.visualization.LineChart(elem);
		} else {
			console.error(chartType + ' is not supported!');
			throw {name: 'UnknownChartTypeError', message: chartType + ' is not supported!'};
		}
	},

	getDataTable: function (report) {
		var header = $.extend(true, [], report.header);
		var data = $.extend(true, [], report.data);
		if (report._chart_type === 'bar') {
			header.push({role: 'annotation'});
			for (var i = 0; i < data.length; i++) {
				var row = data[i];
				row.push(row[0]);
			}
		} else if (report._chart_type.startsWith('timeseries')) {
			for (var i = 0; i < data.length; i++) {
				data[i][0] = new Date(data[i][0]);
			}
		}
		data.unshift(header);
		return new google.visualization.arrayToDataTable(data);
	}
};

module.exports = chartHelper;