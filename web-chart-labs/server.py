import json
import os
from flask import Flask, Response, request, current_app

app = Flask(__name__, static_url_path='', static_folder='public')

app.add_url_rule('/', 'root', lambda: app.send_static_file('index.html'))
app.add_url_rule('/login', 'login', lambda: app.send_static_file('index.html'))
app.add_url_rule('/logout', 'logout', lambda: app.send_static_file('index.html'))
app.add_url_rule('/register', 'register', lambda: app.send_static_file('index.html'))
app.add_url_rule('/dashboards', 'dashboards', lambda: app.send_static_file('index.html'))
app.add_url_rule('/dashboards/<did>', 'dashboard', lambda did: app.send_static_file('index.html'))
app.add_url_rule('/reports/<rid>', 'report', lambda rid: app.send_static_file('index.html'))
# app.add_url_rule('/docs', 'docs', lambda: app.send_static_file('index.html'))
# app.add_url_rule('/help', 'help', lambda: app.send_static_file('index.html'))
app.add_url_rule('/debug', 'debug', lambda: app.send_static_file('index.html'))

if __name__ == '__main__':
    app.run(port=int(os.environ.get("PORT",8000)))