API_NAME = 'Chart Labs API'
# SERVER_NAME = 'chartlabs.herokuapp.com'

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = 'chart-labs'

DOMAIN = {
    'debug': {
        'public_methods': ['GET'],
        'allow_unknown': True,
        'resource_methods': ['GET'],
        'item_methods': ['GET'],
        'schema': {}
    },
    'users': {
        'datasource': {
            'projection': {
                'password_hash': 0
            }
        },
        'public_methods': ['POST'],
        'allow_unknown': True,
        'additional_lookup': {'url': 'regex("[\w]+@[\w]+\.[\w]+")', 'field': 'email'},
        'schema': {
            'display_name': {
                'type': 'string',
                'required': True
            },
            'email': {
                'type': 'string',
                'required': True,
                'unique': True
            },
            'password_hash': {
                'type': 'string',
                'required': True
            },
            'is_superuser': {
                'type': 'boolean',
                'required': True,

            }
        }
    },
    'dashboards': {
        'schema': {
            'user_id': {
                'type': 'objectid',
                'required': True
            },
            'name': {
                'type': 'string',
                'required': True
            }
        }
    },
    'reports': {
        'schema': {
            'user_id': {
                'type': 'objectid',
                'required': True
            },
            'dashboard_id': {
                'type': 'objectid'
            },
            'title': {
                'type': 'string',
                'required': True
            },
            'header': {
                'type': 'list'
            },
            'data': {
                'type': 'list',
                'schema': {
                    'type': 'list'
                }
            },
            '_logs': {
                'type': 'list',
                'schema': {
                    'type': 'string'
                }
            },
            '_is_processed': {
                'type': 'boolean'
            },
            '_is_chartable': {
                'type': 'boolean'
            },
            '_chart_type': {
                'type': 'string',
                'allowed': ['label', 'timeseries_col', 'timeseries_line', 'scatter', 'pie', 'bar', 'stacked', 'line']
            },
        }
    }
}

ALLOWED_FILTERS = ['*']
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-Type', 'If-Match']
AUTH_FIELD = 'user_id'
