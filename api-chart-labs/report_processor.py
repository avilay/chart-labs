from pymongo import MongoClient
import numbers
import dateutil.parser as dtparser
import numpy as np


class ReportProcessor:
    def __init__(self):
        self._report = None
        self._updates = {}

    def _validate(self):
        num_cols = len(self._report['header'])
        data = np.array(self._report['data'])
        if len(data.shape) == 1 or data.shape[1] != num_cols:
            self._updates['_logs'].append('Mismatch in number of columns in header and data')
            return False
        try:
            data[:, 1:].astype(float)
            if num_cols > 11:
                self._updates['_is_chartable'] = False
                self._updates['_logs'].append('This report is not chartable because it has too many columns')
            else:
                self._updates['_is_chartable'] = True
        except ValueError:
            self._updates['_is_chartable'] = False
            self._updates['_logs'].append('This report is not chartable beacuse all y cols are not numeric')
        return True

    # 'label', 'timeseries_col, timeseries_line, scatter, pie, bar, stacked, line
    def _set_chart_type(self):
        num_cols = len(self._report['header'])
        if num_cols == 1:
            self._updates['_chart_type'] = 'label'
            return
        chart_selector = {
            'datetime': {
                '2d': {'few_rows': 'timeseries_col', 'lots_rows': 'timeseries_col'},
                'nd': {'few_rows': 'timeseries_line', 'lots_rows': 'timeseries_line'}
            },
            'number': {
                '2d': {'few_rows': 'scatter', 'lots_rows': 'scatter'},
                'nd': {'few_rows': 'unknown', 'lots_rows': 'unknown'}
            },
            'category': {
                '2d': {'few_rows': 'pie', 'lots_rows': 'bar'},
                'nd': {'few_rows': 'stacked', 'lots_rows': 'line'}
            }
        }
        first_row = self._report['data'][0]
        num_rows = len(self._report['data'])
        try:
            dtparser.parse(first_row[0])
            first_col_dtype = 'datetime'
        except (ValueError, AttributeError, TypeError):
            if isinstance(first_row[0], numbers.Number):
                first_col_dtype = 'number'
            else:
                first_col_dtype = 'category'
        if 1 <= num_rows <= 10:
            row_dims = 'few_rows'
        else:
            row_dims = 'lots_rows'
        if num_cols == 2:
            col_dims = '2d'
        else:
            col_dims = 'nd'
        self._updates['_chart_type'] = chart_selector[first_col_dtype][col_dims][row_dims]

    def process(self, report):
        self._updates = {}
        self._report = report
        self._updates['_logs'] = report['_logs']
        self._updates['_is_processed'] = True
        if self._validate() and self._updates['_is_chartable']:
            self._set_chart_type()
        return self._updates


def main():
    # db = MongoClient('mongodb://happy:orange@ds035653.mongolab.com:35653/heroku_cqznz50r').get_default_database()
    db = MongoClient()['chart-labs']
    reports = db.reports.find({'_is_processed': False})
    processor = ReportProcessor()
    bulk = db.reports.initialize_ordered_bulk_op()
    for report in reports:
        updates = processor.process(report)
        print(report['title'], updates)
        bulk.find({'_id': report['_id']}).update_one({'$set': updates})
    if reports.count() > 0:
        result = bulk.execute()
        print(result)
    else:
        print('No unprocessed reports found')


if __name__ == '__main__':
    main()

