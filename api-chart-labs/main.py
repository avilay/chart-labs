from datetime import datetime
import hashlib
import json
import os.path
import re
from time import sleep
import bcrypt
from eve import Eve
from eve.auth import BasicAuth
from flask import request
from flask_bootstrap import Bootstrap
from eve_docs import eve_docs


class UserAuth(BasicAuth):
    @staticmethod
    def match_password(user, password):
        password = password.encode('utf-8')
        pwdhash = user['password_hash'].encode('utf-8')
        return bcrypt.hashpw(password, pwdhash) == pwdhash

    @staticmethod
    def hash_password(password):
        password = password.encode('utf-8')
        return bcrypt.hashpw(password, bcrypt.gensalt()).decode()

    def check_auth(self, username, password, allowed_roles, resource, method):
        print('{} Authenticating {}/{} for {} /{}'.format(datetime.now(), username, password, method, resource))
        user = app.data.driver.db['users'].find_one({'email': username})
        if not user:
            return False
        if not UserAuth.match_password(user, password):
            return False
        if resource == 'users':
            if method == 'POST':
                return True
            if user['is_superuser']:
                return True
            else:
                match = re.match(r'[\w]+@[\w]+\.[\w]+', os.path.basename(request.path))
                if match and match.group() == user['email']:
                    return True
                else:
                    return False
        else:
            self.set_request_auth_value(user['_id'])
            return True


def scrub_user(req):
    print('Inside scrub_user')
    if 'password' in req.json:
        req.json['password_hash'] = UserAuth.hash_password(req.json['password'])
        del req.json['password']
    req.json['is_superuser'] = False
    print(req.json)


def scrub_report(report):
    report['_logs'] = []
    report['_is_processed'] = False
    report['_is_chartable'] = False
    report['_chart_type'] = None


def create_default_dashboard(users):
    db = app.data.driver.db
    dashboards = []
    for user in users:
        dashboard = {
            'name': 'Default',
            '_is_processed': False,
            '_is_chartable': False,
            '_logs': [],
            '_chart_type': None,
            'user_id': str(user['_id'])
        }
        etag = hashlib.sha1(json.dumps(dashboard).encode('utf-8')).hexdigest()
        dashboard['_etag'] = etag
        dashboard['user_id'] = user['_id']
        dashboards.append(dashboard)
    db.dashboards.insert(dashboards)


def update_dashboard(updated, report):
    print('Updating dashboard')
    db = app.data.driver.db
    did = report['dashboard_id']
    db.dashboards.update({'_id': did}, {'$set': {'_updated': datetime.now()}})


def update_dashboards(reports):
    for report in reports:
        update_dashboard(None, report)


SETTINGS_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'settings.py')
app = Eve(settings=SETTINGS_PATH, auth=UserAuth)
Bootstrap(app)
app.register_blueprint(eve_docs, url_prefix='/docs')
app.on_pre_POST_users += scrub_user
app.on_inserted_users += create_default_dashboard
app.on_pre_GET_debug += lambda p1, p2: sleep(3)
app.on_inserted_reports += update_dashboards
app.on_replaced_reports += update_dashboard
app.on_updated_reports += update_dashboard


if __name__ == '__main__':
    app.run(debug=True)
