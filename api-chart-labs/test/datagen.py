import json
import uuid
import bcrypt
from pymongo import MongoClient
from datetime import datetime, timedelta

# db = MongoClient('mongodb://happy:orange@ds035653.mongolab.com:35653/heroku_cqznz50r').get_default_database()
db = MongoClient()['chart-labs']


def checkdata(cookie):
    return db.test_cookies.find_one()['value'] == cookie


def _gen_auto_report_flds(obj):
    if '_created' not in obj:
        obj['_created'] = datetime.utcnow() - timedelta(days=10)
    if '_updated' not in obj:
        obj['_updated'] = datetime.utcnow() - timedelta(days=5)
    if '_etag' not in obj:
        obj['_etag'] = str(uuid.uuid4()).replace('-', '')
    if '_logs' not in obj:
        obj['_logs'] = []
    if '_is_processed' not in obj:
        obj['_is_processed'] = False
    if '_is_chartable' not in obj:
        obj['_is_chartable'] = False
    if '_chart_type' not in obj:
        obj['_chart_type'] = None


def _gen_auto_flds(obj):
    if '_created' not in obj:
        obj['_created'] = datetime.utcnow() - timedelta(days=10)
    if '_updated' not in obj:
        obj['_updated'] = datetime.utcnow() - timedelta(days=5)
    if '_etag' not in obj:
        obj['_etag'] = str(uuid.uuid4()).replace('-', '')


def gendata():
    collections = db.collection_names()
    if 'users' in collections:
        db.users.remove()
    with open('./data/users.json') as usersf:
        all_users = json.load(usersf)
        for user in all_users:
            user['password_hash'] = bcrypt.hashpw(user['password'].encode('utf-8'), bcrypt.gensalt()).decode()
            del user['password']
            _gen_auto_flds(user)
    db.users.insert(all_users)

    if 'dashboards' in collections:
        db.dashboards.remove()
    with open('./data/dashboards.json') as df:
        all_dashboards = json.load(df)
        for dashboard in all_dashboards:
            user = db.users.find_one({'email': dashboard['user']})
            if not user:
                raise RuntimeError('user {} referenced in dashboards.json is not present in users.json'.format(dashboard['user']))
            dashboard['user_id'] = user['_id']
            del dashboard['user']
            _gen_auto_flds(dashboard)
    db.dashboards.insert(all_dashboards)

    if 'reports' in collections:
        db.reports.remove()
    with open('./data/reports.json') as rf:
        all_reports = json.load(rf)
        for report in all_reports:
            user = db.users.find_one({'email': report['user']})
            if not user:
                raise RuntimeError('user {} referenced in reports.json is not present in users.json'.format(report['user']))
            report['user_id'] = user['_id']
            del report['user']
            dashboard = db.dashboards.find_one({'name': report['dashboard']})
            if not dashboard:
                raise RuntimeError('dashboard {} referenced in reports.json is not present in dashboards.json'.format(report['dashboard']))
            report['dashboard_id'] = dashboard['_id']
            del report['dashboard']
            _gen_auto_report_flds(report)
    db.reports.insert(all_reports)


if __name__ == '__main__':
    gendata()


