import shutil
from datetime import datetime
import os.path as path
import os


prod_root = '/home/avilay/Dropbox/Apps/Heroku/api-chartlabs/'
src_root = '/home/avilay/projects/chart-labs/api-chart-labs'

basename = 'api-chartlabs_{}'.format(datetime.now().strftime('%Y-%m-%d'))
if path.exists(prod_root):
    shutil.make_archive(basename, 'zip', prod_root)
    shutil.rmtree(prod_root)

evedocs_src = path.join(src_root, 'eve_docs')
evedocs_dst = path.join(prod_root, 'eve_docs')
shutil.copytree(evedocs_src, evedocs_dst)

main_src = path.join(src_root, 'main.py')
main_dst = path.join(prod_root, 'main.py')
shutil.copy(main_src, main_dst)

settings_src = path.join(src_root, 'settings.py')
settings_dst = path.join(prod_root, 'settings.py')
shutil.copy(settings_src, settings_dst)

procfile_src = path.join(src_root, 'Procfile')
procfile_dst = path.join(prod_root, 'Procfile')
shutil.copy(procfile_src, procfile_dst)

runtime_src = path.join(src_root, 'runtime.txt')
runtime_dst = path.join(prod_root, 'runtime.txt')
shutil.copy(runtime_src, runtime_dst)

reqms_src = path.join(src_root, 'requirements.txt')
reqms_dst = path.join(prod_root, 'requirements.txt')
shutil.copy(reqms_src, reqms_dst)
