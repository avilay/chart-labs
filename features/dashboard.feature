Feature: Dashboard view


Scenario: Dashboard and report drilldown
	Given I am user with 3 reports set up
	When I visit the Dashboard page
	Then I'll see my latest dashboard on the main page
	And I'll see 3 named tiles for each of my reports.
	When I click on any one of the tiles
	Then I'll see a Report page with an enlarged graph
	And a data table.



Scenario: Report detail
	Given I am a user with 3 reports set up
	When I visit the Dashboard page
