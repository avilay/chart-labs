Feature: Login and Registration
	

Scenario: New visitor comes to the website and registers
	Given I am a new visitor
	When I visit the website
	Then I'll see a landing page with a register button there.
	When I click on the register button
	Then I'll see a register form.
	When I fill out my name, email, and password on the register form
	And click on the register button
	Then I should see a Welcome page with a how-this-works blurb on it.


Scenario: First time user comes to the website from an old browser and is automatically logged in
	Given I am a first-time user
	And am using a browser with a cookie set from a previous session
	When I visit the website
	Then I'll be automatically logged in
	And see a Welcome page with a how-this-works blurb on it.


Scenario: Existing user comes to the website from an old browser and is automatically taken to her dashboard
	Given I am a seasoned user
	And am using a browser with a cookie set from a previous session
	When I visit the website
	Then I'll be automatically logged in
	And see my Dashboard page.


Scenario: Existing user comes to the website from a new browser has to login before being taken to her dashboard
	Given I am a seasoned user
	And am using a new browser (with no cookies set)
	When I visit the website
	Then I'll see a landing page with a login button there.
	When I click on the login button
	Then I'll see login form.
	When I fill out my email and password on the login form
	And click on the login button
	Then I should see my Dashboard page.
